import {
    SET_TOKEN,
    SET_INFO,
} from './types'

export const setToken = token => ({
    type: SET_TOKEN,
    token
})

export const setInfo = info => ({
    type: SET_INFO,
    info
})

