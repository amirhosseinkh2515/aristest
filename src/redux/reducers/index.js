import { combineReducers } from 'redux';
import getInfo from './getInfo'
import getToken from './getToken'


const reducers = combineReducers({
    getToken,
    getInfo
})

export default reducers