import {SET_TOKEN} from '../actions/types'

const initialState = {
    token:"",
}

const getToken = (state = initialState , action={})=>{
    switch (action.type){
        case SET_TOKEN :
            const {token} = action
            return {
                token
            }
            break;
        default : return state
    }
}

export default getToken