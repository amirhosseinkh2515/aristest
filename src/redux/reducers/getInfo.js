import {SET_INFO} from '../actions/types'

const initialState = {
    first_name:"",
    last_name:"",
    gender:"",
    email:"",
    city :""
}

const getInfo = (state = initialState , action={})=>{
    switch (action.type){
        case SET_INFO :
            const {info} = action
            return {
                first_name :info.first_name,
                last_name :info.last_name,
                gender :info.gender,
                email :info.email,
                city : info.city
            }
            break;
        default : return state
    }
}

export default getInfo