import React, { useState } from 'react'
import { Row, Col, Form, Input, Button } from 'antd'
import { useHistory } from "react-router-dom";
import axios from 'axios'
import './ForgetPassword.scss'

const ForgetPassword = () => {
    const [mobile_number, setMobile] = useState("")
    const [err, setErr] = useState("")
    const onMobileChange = (text) => {
        setMobile(text.target.value)
    }
    const enter = (e) => {
        e.preventDefault()
        if (err) {
            console.log("err")
            return ;
        }
        else {
            requestLoginFromApi()
        }

    }
    const requestLoginFromApi = () => {
        axios.post('http://api.pythonist.ir/v1/accounts/registration/step-one/', {
            mobile_number, registrar_type: "expert"
        })
            .then(function (response) {
                console.log(response)
                redirect()

            })
            .catch(function (error) {
                console.log(error.response)
                alert(error.response.data.mobile_number[0])
            })
    }
    const history = useHistory();
    const redirect = () => {
        let path = `Register`;
        history.push(path , mobile_number);
    }
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 10,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };
    const onFinish = values => {
        console.log('Success:', values);
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
        setErr("err")
    };
    return (
        <Form
            {...layout}
            name="basic"
            className="forget-form"
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >
            <Form.Item
                label="Mobile"
                name="Mobile"
                rules={[
                    {
                        required: true,
                        message: 'Please input your Mobile number!',
                    },
                    { min: 11, message: 'Mobile must be minimum of 11 numbers.' },
                ]}
            >
                <Input onChange={onMobileChange} />
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit" onClick={enter}>
                    Submit
          </Button>
            </Form.Item>
        </Form>
    )
}

export default ForgetPassword
