import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Redirect, withRouter } from "react-router-dom";

const SplashScreen = (props) => {
    const [renderSplashscreen, setRenderSplashscreen] = useState(true)
    useEffect(() => {
        setTimeout(() => {
            console.log("TRUE?", props.token !== "")
            if (props.token) {
                props.history.push("./EditProfile")
            }
            else {
                props.history.push("./Login")
            }
        }, 500)
    }, [props])
    return (
        <div> </div>
    )
}

const mapStateToProps = state => ({ token: state.getToken.token, sendedInfo: state.getInfo })
export default withRouter(connect(mapStateToProps, null)(SplashScreen)) 
