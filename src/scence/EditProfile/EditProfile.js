import React, { Component } from 'react'
import { withRouter } from "react-router-dom";
import axios from 'axios'
import { Row, Select, Form, Input, Button } from 'antd';
import { setInfo, setToken } from '../../redux/actions'
import { connect } from 'react-redux'
import './EditProfile.scss'

class EditProfile extends Component {
    constructor(props) {
        super(props)

        this.state = {
            customerSignUp: {
                first_name: this.props.information.first_name,
                last_name: this.props.information.last_name,
                gender: this.props.information.gender,
                email: this.props.information.email,
                city: this.props.information.city,
                invitation_code: ""
            }
        }
        this.harchi()
    }
    harchi(){
        console.log("salam", this.props.information)
        this.setState({
            customerSignUp: {
                first_name: this.props.information.first_name,
                last_name: this.props.information.last_name,
                gender: this.props.information.gender,
                email: this.props.information.email,
                city: this.props.information.city,
                invitation_code: ""
            }
        })
    }

    requestLoginFromApi = () => {
        axios({
            method: 'post',
            url: 'http://api.pythonist.ir/v1/accounts/registration/step-three/',
            data: {
                first_name: this.state.customerSignUp.first_name,
                last_name: this.state.customerSignUp.last_name,
                gender: this.state.customerSignUp.gender,
                email: this.state.customerSignUp.email,
                city: this.state.customerSignUp.city,
                // invitation_code: this.state.customerSignUp.invitation_code,
            },
            headers: {
                "Authorization": 'Bearer ' + this.props.token,
                "role": "expert",
                "Accept-Language": "fa"

            }
        })
            .then(function (response) {
                console.log("havij", response)
                alert(`Registration Completed !!! your new token is ${response.data.access}`)

            })
            .catch(function (error) {
                const err = error.response
                console.log(err)
                if (error.response.data.code) {
                    alert(error.response.data.code)
                }
                else {
                    alert("error")
                }

            })
    }

    handleChange = (e) => {
        const value = e.target.value;
        const name = e.target.name;
        this.setState((state) => ({
            customerSignUp: {
                ...state.customerSignUp,
                [name]: value,
            },
        }));
    };

    enter = () => {
        var regex = /^[0-9]+$/;
        if (!this.state.customerSignUp.city.match(regex)) {
            {
                alert("City should be a number between 1 and 250!");
                return false;
            }
        }
        if (this.state.customerSignUp.first_name !== "" &&
            this.state.customerSignUp.first_name !== " " &&
            this.state.customerSignUp.last_name !== "" &&
            this.state.customerSignUp.last_name !== " " &&
            this.state.customerSignUp.email !== "" &&
            this.state.customerSignUp.city !== "" &&
            this.state.customerSignUp.gender !== "" &&
            this.state.customerSignUp.city > 0 && this.state.customerSignUp.city < 251
        ) {
            this.requestLoginFromApi()
            this.props.setInfo(this.state.customerSignUp)
            return;
        }
        if (this.state.customerSignUp.city <= 0 || this.state.customerSignUp.city > 251) {
            alert("City should be a number between 1 and 250!")
        }
        else {
            console.log("nop")
            alert("please fill all the rquired fildes")
        }
    }

    layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 10,
        },
    };
    tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };
    onFinish = values => {
        console.log('Success:', values);
    };

    onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };
    logOut = () => {
        this.props.setToken("")
        let path = `/Login`;
        this.props.history.push(path)

    }
    render() {
        console.log(this.state.customerSignUp, "first-name")
        return (
            <Form
                {...this.layout}
                name="basic"
                className="edit-form"
                initialValues={{
                    remember: true,
                }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label="First name"
                    name="first_name"
                    value={this.state.customerSignUp.first_name}
                    onChange={this.handleChange}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your First name!',
                        },
                    ]}
                >
                    <Input name="first_name" defaultValue={this.state.customerSignUp.first_name} />
                </Form.Item>
                <Form.Item
                    label="Last name"
                    name="last_name"
                    value={this.state.customerSignUp.last_name}
                    onChange={this.handleChange}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Last name!',
                        },
                    ]}
                >
                    <Input name="last_name" defaultValue={this.state.customerSignUp.last_name}/>
                </Form.Item>
                <Form.Item
                    label="Gender"
                    name="gender"
                    rules={[
                        {
                            required: true,
                            message: 'Please select your gender!',
                        },
                    ]}
                >
                    <Select defaultValue={this.state.customerSignUp.gender} name="gender" onChange={(value) => this.setState((state) => ({
                        customerSignUp: {
                            ...state.customerSignUp,
                            gender: value,
                        },
                    }))}>
                        <Select.Option value="m">Male</Select.Option>
                    <Select.Option value="f">Female</Select.Option>
                    </Select>
                </Form.Item>

            <Form.Item
                label="Email"
                name="email"
                value={this.state.customerSignUp.email}
                onChange={this.handleChange}
                rules={[
                    {
                        required: true,
                        message: 'Please input your Email!',
                    },
                ]}
            >
                <Input name="email" defaultValue={this.state.customerSignUp.email}/>
            </Form.Item>
            <Form.Item
                label="City"
                name="city"
                value={this.state.customerSignUp.city}
                onChange={this.handleChange}
                rules={[
                    {
                        required: true,
                        message: 'City should be a number between 1 and 250!',
                    },
                ]}
            >
                <Input name="city" defaultValue={this.state.customerSignUp.city}/>
            </Form.Item>
            <Form.Item
                label="Invitation code"
                name="invitation_code"
                value={this.state.customerSignUp.invitation_code}
                onChange={this.handleChange}
            >
                <Input name="invitation_code" defaultValue={this.state.customerSignUp.invitation_code}/>
            </Form.Item>

            <Form.Item {...this.tailLayout}>
                <Button type="primary" htmlType="submit" onClick={this.enter}>
                    Submit
                    </Button>
            </Form.Item>
            <Form.Item {...this.tailLayout}>
                <Button type="danger" htmlType="submit" onClick={this.logOut}>
                    Log out
                    </Button>
            </Form.Item>
            </Form >
        )
    }

}
const mapStateToProps = state => ({ information: state.getInfo })
const mapDispatchToProps = dispatch => {
    return {
        setInfo: info => {
            dispatch(setInfo(info))
        },
        setToken: token => {
            dispatch(setToken(token))
        },
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditProfile)) 
