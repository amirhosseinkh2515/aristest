import React, { useState } from 'react'
import { useHistory } from "react-router-dom";
import axios from 'axios'
import { Row, Col, Form, Input, Button } from 'antd'
import { setToken } from '../../redux/actions'
import { connect } from 'react-redux'
import './Login.scss'

const Login = (props) => {
    const [customerSignUp, setCustomerSignUp] = useState(
        { username: '', password: '' }
    );

    const handleChange = (event) => {
        setCustomerSignUp({ ...customerSignUp, [event.target.name]: event.target.value })
    }
    const enter = () => {
        if (customerSignUp.password !== "" && customerSignUp.username !== "") {
            requestLoginFromApi()
        }

        else if (customerSignUp.username === "") {
            alert("please enter your username")
        }
        else if (customerSignUp.password === "") {
            alert("please enter your password")
        }
        else {
            console.log("nop")
        }
    }
    const requestLoginFromApi = () => {
        axios({
            method: 'post',
            url: 'http://api.pythonist.ir/v1/accounts/login/',
            data: { username: customerSignUp.username, password: customerSignUp.password },
            headers: {
                "role": "expert",
                "Accept-Language": "fa"

            }
        })
            .then(function (response) {
                console.log(response)
                redirect(response.data)

            })
            .catch(function (error) {
                console.log(error.response)
                alert(error.response.data.detail)
            })
    }
    const history = useHistory();
    const redirect = (response) => {
        console.log("response", response)
        props.setToken(response.access)
        let path = `EditProfile`;
        history.push(path, response);
    }
    const register = () => {
        let path = `ForgetPassword`;
        history.push(path);
    }
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 10,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };
    const onFinish = values => {
        console.log('Success:', values);
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };
    return (
        <Form
            {...layout}
            name="basic"
            className="forget-form"
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >
            <Form.Item
                label="Phone number"
                name="username"
                value={customerSignUp.username}
                onChange={handleChange}
                rules={[
                    {
                        required: true,
                        message: 'Please input your Phone number!',
                    },
                ]}
            >
                <Input name="username" />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                value={customerSignUp.password}
                onChange={handleChange}
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                ]}
            >
                <Input.Password name="password" />
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit" onClick={enter}>
                    Login
                </Button>

            </Form.Item>
            <Form.Item {...tailLayout}>
                <Button type="danger" htmlType="submit" onClick={register}>
                    Register
                </Button>
            </Form.Item>
        </Form>
    )
}
const mapDispatchToProps = dispatch => {
    return {
        setToken: token => {
            dispatch(setToken(token))
        }
    }
}
export default connect(null, mapDispatchToProps)(Login)
