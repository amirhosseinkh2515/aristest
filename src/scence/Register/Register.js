import React, { useState } from 'react'
import { useHistory } from "react-router-dom";
import axios from 'axios'
import { Row, Col, Form, Input, Button } from 'antd'
import './Register.scss'

const Register = ({location}) => {
    console.log(location.state)
    const [customerSignUp, setCustomerSignUp] = useState(
        { code: '', password: '', repassword: ''}
    );

    const handleChange = (event) => {
        setCustomerSignUp({...customerSignUp, [event.target.name]: event.target.value})
    }
    const enter = () => {
        if(customerSignUp.password === customerSignUp.repassword && customerSignUp.code !== ""){
            requestLoginFromApi()
        }
        else if(customerSignUp.password !== customerSignUp.repassword){
            alert("You did not repeat the password correctly")
        }
        else if(customerSignUp.code === ""){
            alert("please enter a valid code")
        }
        else{
            console.log("nop")
        }
    }
    const requestLoginFromApi = () => {
        axios({
            method: 'post',
            url: 'http://api.pythonist.ir/v1/accounts/registration/step-two/',
            data: {code : customerSignUp.code , password : customerSignUp.password},
            headers: {
              "role" : "expert" ,
              "Accept-Language": "fa"

            }
          })
            .then(function (response) {
                console.log(response)
                redirect(response.data)

            })
            .catch(function (error) {
                console.log(error.response)
                alert(error.response.data.code)
            })
    }
    const history = useHistory();
    const redirect = (response) => {
        console.log("response",response)
        let path = `EditProfile`;
        history.push(path , response);
    }
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 10,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };
    const onFinish = values => {
        console.log('Success:', values);
      };
    
      const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
      };
    return (
        <Form
            {...layout}
            name="basic"
            className="forget-form"
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >
            <Form.Item
                label="Code"
                name="code"
                value={customerSignUp.code}
                onChange={handleChange}
                rules={[
                    {
                        required: true,
                        message: 'Please input your Code!',
                    },
                ]}
            >
                <Input name="code"/>
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                value={customerSignUp.password}
                onChange={handleChange}
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                ]}
            >
                <Input.Password name="password"/>
            </Form.Item>

            <Form.Item
                label="Repeat Password"
                name="repassword"
                value={customerSignUp.repassword}
                onChange={handleChange}
                rules={[
                    {
                        required: true,
                        message: 'Please repeat your password!',
                    },
                ]}
            >
                <Input.Password name="repassword"/>
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit" onClick={enter}>
                    Submit
          </Button>
            </Form.Item>
        </Form>
    )
}

export default Register
