import React, { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Spinner from "./components/GlobalUsage/Spinner/Spinner";
import Register from './scence/Register/Register'
import EditProfile from './scence/EditProfile/EditProfile'
import ForgetPassword from './scence/ForgetPassword/ForgetPassword'
import Login from './scence/Login/Login'
import SplashScreen from './scence/SplashScreen/SplashScreen'
import 'antd/dist/antd.css';
import './App.scss'
import { Row, Col } from 'antd';

function App() {
  return (
    <BrowserRouter>
      <Suspense fallback={<Spinner />}>
        <Row className="main">
          <Col span={24}>
            <Switch >
              <Route exact path='/' component={SplashScreen} />
              <Route exact path='/Login' component={Login} />
              <Route path='/ForgetPassword' component={ForgetPassword} />
              <Route path='/Register' component={Register} />
              <Route path='/EditProfile' component={EditProfile} />
            </Switch>
          </Col>
        </Row>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
