import React from "react";
import "./Spinner.scss";

function Spinner() {
  return (
    <div className="spinner">
      please wait
    </div>
  );
}

export default Spinner;
